FROM ubuntu:focal

RUN mkdir /quoridor
RUN apt update && apt install -y \
    libgssapi-krb5-2 \
    && rm -rf /var/lib/apt/lists/*

ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT "1"

COPY Quoridor.AiTester/bin/release/net5.0/linux-x64/publish/Quoridor.AiTester /usr/local/bin/ai-tester

CMD ai-tester